require "authority_badger/version"

require 'authority_badger/acts_as_token'
require 'authority_badger/acts_as_token_balance'
require 'authority_badger/acts_as_token_transaction'
require 'authority_badger/acts_as_token_price'
require 'authority_badger/has_tokens'

module AuthorityBadger
  # Your code goes here...
end

ActiveRecord::Base.send(:include, AuthorityBadger::ActsAsToken)
ActiveRecord::Base.send(:include, AuthorityBadger::ActsAsTokenBalance)
ActiveRecord::Base.send(:include, AuthorityBadger::ActsAsTokenTransaction)
ActiveRecord::Base.send(:include, AuthorityBadger::ActsAsTokenPrice)
ActiveRecord::Base.send(:include, AuthorityBadger::HasTokens)
