# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'authority_badger/version'

Gem::Specification.new do |spec|
  spec.name          = "authority_badger"
  spec.version       = AuthorityBadger::VERSION
  spec.authors       = ["Oliver Brooks"]
  spec.email         = ["oli@valobox.com"]
  spec.description   = "Use tokens to access services"
  spec.summary       = "Use tokens to access services"
  spec.homepage      = "https://bitbucket.org/completelynovel/authority_badger"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "rails"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
